package ie.eightwest;

import java.util.*;

public class UserDaoInMemory implements UserDao {

    protected HashMap<Long, User> userMap;

    public UserDaoInMemory() {
        userMap = new HashMap<>();
    }
    public User addUser(User user) throws UserDaoException {

        if (user.getId() == -1) {
            // assign a new id and add to map
            Set<Long> keys = userMap.keySet();
            long lastId = keys.stream()
                    .max(Comparator.comparing(Long::valueOf))
                    .get();
            user.setId(lastId + 1);
        } else {
            if (userMap.containsKey(user.getId())) {
                // id already exists - throw exception
                throw new UserDaoException("User already exists");
            }
        }
        userMap.put(user.getId(), user);
        return user;
    }

    public User updateUser(User user) throws UserDaoException {
        if (userMap.containsKey(user.getId())) {
            userMap.replace(user.getId(), user);
            return user;
        } else {
            throw new UserDaoException("Can't update user that doesn't exist");
        }
    }

    public void deleteUser(long id) throws UserDaoException {
        if (userMap.containsKey(id)) {
                userMap.remove(id);
        } else {
            throw new UserDaoException("Can't delete user that doesn't exist");
        }
    }
    public Collection<User> getUsers() {
        return userMap.values();
    }

    public User getUser(long id) throws UserDaoException {
        if (userMap.containsKey(id)) {
            return userMap.get(id);
        } else {
            throw new UserDaoException("User not found:" + id);
        }
    }

    @Override
    public void close() throws UserDaoException {
        userMap.clear();
    }

    @Override
    public String toString() {
        return userMap.toString();
    }

    public static void main(String[] args) {

        // create an interface rather than a specific implementation
        //UserDaoInMemory udb = new UserDaoInMemory();
        UserDao udb = new UserDaoInMemory();


        User user = new User(1, "Aidan", "aidan@gmail.com", true);
        try {
            udb.addUser(user);
        } catch (UserDaoException e) {
            e.printStackTrace();
        }
        User user2 = new User(2, "Bob", "bob@gmail.com", true);
        try {
            udb.addUser(user2);
        } catch (UserDaoException e) {
            e.printStackTrace();
        }

        System.out.println(udb);

        Collection<User> users = udb.getUsers();

        for (User u:users) {

            System.out.println(u.getName());

        }


        User found = null;
        try {
            found = udb.getUser(1);
        } catch (UserDaoException e) {
            e.printStackTrace();
        }

        System.out.println(found);

        User notFound = null;
        try {
            notFound = udb.getUser(99);
        } catch (UserDaoException e) {
            e.printStackTrace();
        }

        System.out.println(notFound);


        System.out.println(users);
        try {
            udb.deleteUser(99);
        } catch (UserDaoException e) {
            e.printStackTrace();
        }

        users = udb.getUsers();

        System.out.println(users);

        User newUser = new User(4, "New User", "new@gmail.com", true);

        try {
            udb.addUser(newUser);
            udb.addUser(newUser);
        } catch (UserDaoException e) {
            e.printStackTrace();
        }

        users = udb.getUsers();

        System.out.println(users);
        newUser.setName("changed");

        User userToUpdate = new User(99, "update", "update@gmail.com", false);

        try {
            udb.updateUser(userToUpdate);
        } catch (UserDaoException e) {
            e.printStackTrace();
        }

        users = udb.getUsers();

        System.out.println(users);


        System.out.println("----------------------");
        User unull = new User();

        System.out.println(unull);


        System.out.println("-----------------------");

        try {
            User n1 = new User("new user", "new@gmail.com", false);
            n1 = udb.addUser(n1);

            users = udb.getUsers();
            System.out.println(users);

            System.out.println(n1);


            User n2 = new User(99, "another new user", "another@gmail.com", true);
            n2 = udb.addUser(n2);

            users = udb.getUsers();
            System.out.println(users);
            System.out.println(n2);

            User n3 = new User(99, "yet another new user", "yanu@gmail.com", true);
            udb.addUser(n3);

            users = udb.getUsers();
            System.out.println(users);
            System.out.println(n3);
















        } catch (UserDaoException e) {
            e.printStackTrace();
        }


    }
}
