package ie.eightwest;

import org.junit.Test;
import static org.junit.Assert.*;

public class UserTest {

    @Test
    public void create() {
        User u = new User("Aidan", "aidan@gmail.com", true);

        assertNotNull(u);

        assertEquals(u.getId(), -1);

    }
}
