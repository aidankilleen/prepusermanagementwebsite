package ie.eightwest;

import org.junit.Test;
import static org.junit.Assert.*;


public class HomeControllerTest {

    @Test
    public void testHome() {
        HomeController hc = new HomeController();

        double r = Math.random();

        System.out.println(r);
        if (r < 0.86) {
            assertNotNull(hc);
        } else {
            assertNull(hc);
        }
    }
}
